﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddForm
{
    public partial class Form1 : Form
    {
        public WeaponClass.AbstractWeapon weapon;
        public delegate void EditingFinished(WeaponClass.AbstractWeapon weapon);
        public static EditingFinished editingFinished;
        public Form1()
        {
            InitializeComponent();
        }
        public Form1(WeaponClass.AbstractWeapon weapon) : this()
        {
            if (weapon != null)
            {
                switch (weapon.GetType().Name)
                {
                    case "Rifle":
                        {
                            comboBox1.SelectedIndex = 0;
                            break;
                        }
                    case "MachineGun":
                        {
                            comboBox1.SelectedIndex = 1;
                            break;
                        }
                    case "Knife":
                        {
                            comboBox1.SelectedIndex = 2;
                            break;
                        }
                    case "Rapier":
                        {
                            comboBox1.SelectedIndex = 3;
                            break;
                        }
                }
                textBox1.Text = weapon.Name;
                textBox2.Text = weapon.Cost.ToString();
                textBox3.Text = weapon.Quantity.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == null || textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
            {
                MessageBox.Show("Please fill every field");
            }
            else
            {
                switch (comboBox1.SelectedIndex)
                {
                    case 0:
                        weapon = new WeaponClass.Rifle();
                        break;
                    case 1:
                        weapon = new WeaponClass.Machinegun();
                        break;
                    case 2:
                        weapon = new WeaponClass.Knife();
                        break;
                    case 3:
                        weapon = new WeaponClass.Rapier();
                        break;
                }
                weapon.Name = textBox1.Text;
                weapon.Cost = Int32.Parse(textBox2.Text);
                weapon.Quantity = Int32.Parse(textBox3.Text);

                if (weapon.Cost <= 0 || weapon.Quantity <= 0)
                {
                    MessageBox.Show("Weapon cost and quantity cannot be lower than or equal to 0");
                }
                else
                {
                    editingFinished.Invoke(weapon);
                    Close();
                }
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            editingFinished.Invoke(null);
            Close();
        }
    }
}
