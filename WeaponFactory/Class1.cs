﻿using System;
using WeaponClass;

namespace WeaponFactory
{
    public class WeaponFactory
    {
        public Rifle ProvideRifle()
        {
            Rifle rifle = new Rifle();
            rifle.Name = "M-16";
            rifle.FireRate = 700;
            rifle.Weight = 3.6f;
            rifle.Range = 800;
            rifle.Cost = 7000;
            rifle.Quantity = 1;
            return rifle;
        }
        public Rifle ProvideRifle(string name, int fireRate, float weight, float range, int cost, int quantity)
        {
            Rifle rifle = new Rifle();
            rifle.Name = name;
            rifle.FireRate = fireRate;
            rifle.Weight = weight;
            rifle.Range = range;
            rifle.Cost = cost;
            rifle.Quantity = quantity;
            return rifle;
        }
        public Knife ProvideKnife()
        {
            Knife knife = new Knife();
            knife.Weight = 0.1f;
            knife.DamageType = "Cutting";
            knife.Length = 0.2f;
            knife.Name = "Army Knife";
            knife.Cost = 300;
            knife.Quantity = 1;
            return knife;
        }
        public Knife ProvideKnife(string name, float weight, string damageType, float length, int cost, int quantity)
        {
            Knife knife = new Knife();
            knife.Name = name;
            knife.Weight = weight;
            knife.DamageType = damageType;
            knife.Length = length;
            knife.Cost = cost;
            knife.Quantity = quantity;
            return knife;
        }
    }
}
